﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ColeParmerDIoTestRig
{
    public class DigitalInterfaceCP18200_10_Test : Bytronic.IO.ColeParmer.DigitalIoInterface18200_10
    {
        private int testTicker = 0;
        private int testTickerMax = 100;


        private const string LINE_OUT_PULSE_NAME = "PC Output Pulse";
        private const int LINE_OUT_PULSE_IDX = 7;

        private const string LINE_IN_SENSOR_1_NAME = "Sensor 1";
        private const int LINE_IN_SENSOR_1_IDX = 0;

        private const string LINE_IN_SENSOR_2_NAME = "Sensor 2";
        private const int LINE_IN_SENSOR_2_IDX = 1;

        public event Bytronic.IO.LineLevelChangedEventHandler Sensor1LevelChanged;
        public event Bytronic.IO.LineLevelChangedEventHandler Sensor2LevelChanged;
        public event Bytronic.IO.LineLevelChangedEventHandler OutputPulsed;


        public DigitalInterfaceCP18200_10_Test()
            : base()
        {
            // Make some nice line names.
            this.InputLines[LINE_IN_SENSOR_1_IDX].Name = LINE_IN_SENSOR_1_NAME;
            this.InputLines[LINE_IN_SENSOR_2_IDX].Name = LINE_IN_SENSOR_2_NAME;
            this.OutputLines[LINE_OUT_PULSE_IDX].Name = LINE_OUT_PULSE_NAME;

            // Listen for input lines changing their values.
            this.InputLevelChanged += DigitalInterfaceCP18200_10_Test_InputLevelChanged;

            // We can also listen for output lines changing their values here,
            // if we want to.
            this.OutputLevelChanged += DigitalInterfaceCP18200_10_Test_OutputLevelChanged;
        }


        private void DigitalInterfaceCP18200_10_Test_InputLevelChanged(object sender, Bytronic.IO.LineLevelChangedEventArgs e)
        {
            if (e.Line.LineIndex == LINE_IN_SENSOR_1_IDX)
                this.OnSensor1LevelChanged(e);
            else if (e.Line.LineIndex == LINE_IN_SENSOR_2_IDX)
                this.OnSensor2LevelChanged(e);
            else
            {
                // Other handlers here...
            }
        }


        void DigitalInterfaceCP18200_10_Test_OutputLevelChanged(object sender, Bytronic.IO.LineLevelChangedEventArgs e)
        {
            if (e.Line.LineIndex == LINE_OUT_PULSE_IDX)
                this.OnOutputPulsed(e);
            else
            {
                // Other handlers here...
            }
        }


        protected virtual void OnOutputPulsed(Bytronic.IO.LineLevelChangedEventArgs e)
        {
            if (this.OutputPulsed != null)
                this.OutputPulsed(this, e);
        }


        protected virtual void OnSensor1LevelChanged(Bytronic.IO.LineLevelChangedEventArgs e)
        {
            if (this.Sensor1LevelChanged != null)
                this.Sensor1LevelChanged(this, e);
        }


        protected virtual void OnSensor2LevelChanged(Bytronic.IO.LineLevelChangedEventArgs e)
        {
            if (this.Sensor2LevelChanged != null)
                this.Sensor2LevelChanged(this, e);
        }


        protected override void OnReadWriteCompleted(EventArgs e)
        {
            // Pulse one of the output lines.
            if (++this.testTicker >= this.testTickerMax)
            {
                this.OutputLines[LINE_OUT_PULSE_IDX].CurrentLevel = !this.OutputLines[LINE_OUT_PULSE_IDX].CurrentLevel;
                this.testTicker = 0;
            }

            base.OnReadWriteCompleted(e);
        }


        #region Example getters and setters.
        /// <summary>
        /// Return the current value of sensor 1.
        /// </summary>
        public bool Sensor1Level
        {
            get { return this.InputLines[LINE_IN_SENSOR_1_IDX].CurrentLevel; }
        }

        /// <summary>
        /// Return the current value of sensor 2.
        /// </summary>
        public bool Sensor2Level
        {
            get { return this.InputLines[LINE_IN_SENSOR_2_IDX].CurrentLevel; }
        }

        // Gets/Sets the level of output line 6.
        public bool Output6
        {
            get { return this.OutputLines[6].CurrentLevel; }
            set { this.OutputLines[6].CurrentLevel = value; }
        }
        #endregion
    }
}
