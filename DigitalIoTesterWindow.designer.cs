﻿namespace ColeParmerDIoTestRig
{
    partial class DigitalIoTesterWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DigitalIoTesterWindow));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.buttonSelectInterfaceClass = new Bytronic.GUI.MultiStateButton();
            this.buttonClose = new Bytronic.GUI.StandardButton();
            this.buttonStartStop = new Bytronic.GUI.MultiStateButton();
            this.buttonShowHideMonitor = new Bytronic.GUI.MultiStateButton();
            this.statusLabel1 = new Bytronic.GUI.StatusLabel();
            this.buttonSensor1 = new Bytronic.GUI.MultiStateButton();
            this.buttonSensor2 = new Bytronic.GUI.MultiStateButton();
            this.buttonOutputPulse = new Bytronic.GUI.MultiStateButton();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel1.Controls.Add(this.buttonOutputPulse, 3, 3);
            this.tableLayoutPanel1.Controls.Add(this.buttonSensor2, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.buttonSensor1, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.buttonSelectInterfaceClass, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.buttonClose, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.buttonStartStop, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.buttonShowHideMonitor, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.statusLabel1, 0, 4);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 6;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(512, 288);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // buttonSelectInterfaceClass
            // 
            this.buttonSelectInterfaceClass.AutoAdvanceState = false;
            this.tableLayoutPanel1.SetColumnSpan(this.buttonSelectInterfaceClass, 4);
            this.buttonSelectInterfaceClass.CurrentStateIndex = -1;
            this.buttonSelectInterfaceClass.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonSelectInterfaceClass.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSelectInterfaceClass.Location = new System.Drawing.Point(3, 3);
            this.buttonSelectInterfaceClass.Name = "buttonSelectInterfaceClass";
            this.buttonSelectInterfaceClass.Size = new System.Drawing.Size(506, 34);
            this.buttonSelectInterfaceClass.TabIndex = 1;
            this.buttonSelectInterfaceClass.Text = "Select Interface Class";
            this.buttonSelectInterfaceClass.UseVisualStyleBackColor = true;
            // 
            // buttonClose
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.buttonClose, 4);
            this.buttonClose.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonClose.Location = new System.Drawing.Point(3, 251);
            this.buttonClose.Name = "buttonClose";
            this.buttonClose.Size = new System.Drawing.Size(506, 34);
            this.buttonClose.TabIndex = 3;
            this.buttonClose.Text = "Close";
            this.buttonClose.UseVisualStyleBackColor = true;
            this.buttonClose.Click += new System.EventHandler(this.buttonClose_Click);
            // 
            // buttonStartStop
            // 
            this.buttonStartStop.AutoAdvanceState = false;
            this.tableLayoutPanel1.SetColumnSpan(this.buttonStartStop, 4);
            this.buttonStartStop.CurrentStateIndex = -1;
            this.buttonStartStop.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonStartStop.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonStartStop.Location = new System.Drawing.Point(3, 43);
            this.buttonStartStop.Name = "buttonStartStop";
            this.buttonStartStop.Size = new System.Drawing.Size(506, 34);
            this.buttonStartStop.TabIndex = 4;
            this.buttonStartStop.Text = "Start/Stop";
            this.buttonStartStop.UseVisualStyleBackColor = true;
            this.buttonStartStop.Click += new System.EventHandler(this.buttonStartStop_Click);
            // 
            // buttonShowHideMonitor
            // 
            this.buttonShowHideMonitor.AutoAdvanceState = false;
            this.tableLayoutPanel1.SetColumnSpan(this.buttonShowHideMonitor, 4);
            this.buttonShowHideMonitor.CurrentStateIndex = -1;
            this.buttonShowHideMonitor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonShowHideMonitor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonShowHideMonitor.Location = new System.Drawing.Point(3, 83);
            this.buttonShowHideMonitor.Name = "buttonShowHideMonitor";
            this.buttonShowHideMonitor.Size = new System.Drawing.Size(506, 34);
            this.buttonShowHideMonitor.TabIndex = 5;
            this.buttonShowHideMonitor.Text = "Show Monitor";
            this.buttonShowHideMonitor.UseVisualStyleBackColor = true;
            this.buttonShowHideMonitor.Click += new System.EventHandler(this.buttonShowHideMonitor_Click);
            // 
            // statusLabel1
            // 
            this.statusLabel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(203)))), ((int)(((byte)(203)))), ((int)(((byte)(203)))));
            this.tableLayoutPanel1.SetColumnSpan(this.statusLabel1, 4);
            this.statusLabel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.statusLabel1.Location = new System.Drawing.Point(3, 208);
            this.statusLabel1.Name = "statusLabel1";
            this.statusLabel1.Size = new System.Drawing.Size(506, 40);
            this.statusLabel1.TabIndex = 6;
            this.statusLabel1.Text = "statusLabel1";
            this.statusLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // buttonSensor1
            // 
            this.buttonSensor1.AutoAdvanceState = false;
            this.buttonSensor1.CurrentStateIndex = -1;
            this.buttonSensor1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonSensor1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSensor1.Location = new System.Drawing.Point(3, 123);
            this.buttonSensor1.Name = "buttonSensor1";
            this.buttonSensor1.Size = new System.Drawing.Size(94, 82);
            this.buttonSensor1.TabIndex = 7;
            this.buttonSensor1.Text = "Sensor 1";
            this.buttonSensor1.UseVisualStyleBackColor = true;
            // 
            // buttonSensor2
            // 
            this.buttonSensor2.AutoAdvanceState = false;
            this.buttonSensor2.CurrentStateIndex = -1;
            this.buttonSensor2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonSensor2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSensor2.Location = new System.Drawing.Point(103, 123);
            this.buttonSensor2.Name = "buttonSensor2";
            this.buttonSensor2.Size = new System.Drawing.Size(94, 82);
            this.buttonSensor2.TabIndex = 8;
            this.buttonSensor2.Text = "Sensor 2";
            this.buttonSensor2.UseVisualStyleBackColor = true;
            // 
            // buttonOutputPulse
            // 
            this.buttonOutputPulse.AutoAdvanceState = false;
            this.buttonOutputPulse.CurrentStateIndex = -1;
            this.buttonOutputPulse.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonOutputPulse.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonOutputPulse.Location = new System.Drawing.Point(415, 123);
            this.buttonOutputPulse.Name = "buttonOutputPulse";
            this.buttonOutputPulse.Size = new System.Drawing.Size(94, 82);
            this.buttonOutputPulse.TabIndex = 9;
            this.buttonOutputPulse.Text = "Output Pulse";
            this.buttonOutputPulse.UseVisualStyleBackColor = true;
            // 
            // DigitalIoTesterWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(512, 288);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "DigitalIoTesterWindow";
            this.Text = "Digital I/O Tester";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.DigitalIoTesterWindow_FormClosing);
            this.Load += new System.EventHandler(this.DigitalIoTesterWindow_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private Bytronic.GUI.MultiStateButton buttonSelectInterfaceClass;
        private Bytronic.GUI.StandardButton buttonClose;
        private Bytronic.GUI.MultiStateButton buttonStartStop;
        private Bytronic.GUI.MultiStateButton buttonShowHideMonitor;
        private Bytronic.GUI.StatusLabel statusLabel1;
        private Bytronic.GUI.MultiStateButton buttonOutputPulse;
        private Bytronic.GUI.MultiStateButton buttonSensor2;
        private Bytronic.GUI.MultiStateButton buttonSensor1;
    }
}