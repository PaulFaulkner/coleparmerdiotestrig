﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ColeParmerDIoTestRig
{
    public partial class DigitalIoTesterWindow : Form
    {
        private const int DELAY_LOAD_DELAY = 500;   //ms

        private DigitalInterfaceCP18200_10_Test digitalIoInterface = null;
        
        private Bytronic.GUI.DigitalIoWindow digitalIoWindow = null;

        private const int BTN_IO_CLASS_STATE_NOT_READY = 0;
        private const int BTN_IO_CLASS_STATE_READY = 1;

        private const int BTN_IO_START_STOP_STATE_NOT_READY = 0;
        private const int BTN_IO_START_STOP_STATE_RUNNING = 1;
        private const int BTN_IO_START_STOP_STATE_STOPPED = 2;

        private const int BTN_IO_MONITOR_STATE_NOT_READY = 0;
        private const int BTN_IO_MONITOR_STATE_VISIBLE = 1;
        private const int BTN_IO_MONITOR_STATE_HIDDEN = 2;

        private const int BTN_SENSOR_1_LEVEL_STATE_LOW = 0;
        private const int BTN_SENSOR_1_LEVEL_STATE_HIGH = 1;

        private const int BTN_SENSOR_2_LEVEL_STATE_LOW = 0;
        private const int BTN_SENSOR_2_LEVEL_STATE_HIGH = 1;

        private const int BTN_OUTPUT_PULSE_STATE_LOW = 0;
        private const int BTN_OUTPUT_PULSE_STATE_HIGH = 1;

        private delegate void RefreshButtonStatesCallback();
        private delegate void ShowMessageCallback(string text);
        private delegate void ShowErrorCallback(string text);


        public DigitalIoTesterWindow()
            : base()
        {
            this.InitializeComponent();

            this.buttonSelectInterfaceClass.AddState("No I/O Interface Created.", Bytronic.GUI.StandardColours.ComponentNotReady);
            this.buttonSelectInterfaceClass.AddState("Interface Created.", Bytronic.GUI.StandardColours.ComponentReady);

            this.buttonStartStop.AddState("Start", Bytronic.GUI.StandardColours.ComponentNotReady);
            this.buttonStartStop.AddState("Stop", Bytronic.GUI.StandardColours.BooleanValueHigh);
            this.buttonStartStop.AddState("Start", Bytronic.GUI.StandardColours.BooleanValueLow);

            this.buttonShowHideMonitor.AddState("Show I/O Monitor", Bytronic.GUI.StandardColours.ComponentNotReady);
            this.buttonShowHideMonitor.AddState("Hide I/O Monitor", Bytronic.GUI.StandardColours.BooleanValueHigh);
            this.buttonShowHideMonitor.AddState("Show I/O Monitor", Bytronic.GUI.StandardColours.BooleanValueLow);

            this.buttonSensor1.AddState("Sensor 1", Bytronic.GUI.StandardColours.BooleanValueLow);
            this.buttonSensor1.AddState("Sensor 1", Bytronic.GUI.StandardColours.BooleanValueHigh);

            this.buttonSensor2.AddState("Sensor 2", Bytronic.GUI.StandardColours.BooleanValueLow);
            this.buttonSensor2.AddState("Sensor 2", Bytronic.GUI.StandardColours.BooleanValueHigh);

            this.buttonOutputPulse.AddState("Output Pulse", Bytronic.GUI.StandardColours.BooleanValueLow);
            this.buttonOutputPulse.AddState("Output Pulse", Bytronic.GUI.StandardColours.BooleanValueHigh);
        }


        private void DigitalIoTesterWindow_Load(object sender, EventArgs e)
        {
            this.ShowMessage(null);

            // I like to move the typical _Load() stuff into a delay-loaded
            // methoed so the user can see the UI transition from an
            // unitialised state to an initialised state.
            System.Threading.Thread delayLoadThread = new System.Threading.Thread(this.DelayLoad);
            delayLoadThread.Start();

            this.RefreshButtonStates();
        }


        private void DelayLoad()
        {
            System.Threading.Thread.Sleep(DELAY_LOAD_DELAY);

            // Create our main I/O object.
            this.digitalIoInterface = new DigitalInterfaceCP18200_10_Test();
            this.buttonSelectInterfaceClass.SetStateText(
                BTN_IO_CLASS_STATE_READY,
                this.digitalIoInterface.GetType().ToString()
                );

            // Listen for input events.
            this.digitalIoInterface.Sensor1LevelChanged += digitalIoInterface_Sensor1LevelChanged;
            this.digitalIoInterface.Sensor2LevelChanged += digitalIoInterface_Sensor2LevelChanged;

            // And listen for our output pulse changing.
            this.digitalIoInterface.OutputPulsed += digitalIoInterface_OutputPulsed;

            this.RefreshButtonStates();
        }


        

        #region Handle Digital I/O inputs.
        void digitalIoInterface_Sensor1LevelChanged(object sender, Bytronic.IO.LineLevelChangedEventArgs e)
        {
            this.buttonSensor1.SetStateIndex(e.LineLevel ? BTN_SENSOR_1_LEVEL_STATE_HIGH : BTN_SENSOR_1_LEVEL_STATE_LOW);
        }


        void digitalIoInterface_Sensor2LevelChanged(object sender, Bytronic.IO.LineLevelChangedEventArgs e)
        {
            this.buttonSensor2.SetStateIndex(e.LineLevel ? BTN_SENSOR_1_LEVEL_STATE_HIGH : BTN_SENSOR_1_LEVEL_STATE_LOW);
        }
        #endregion


        #region Handle Digital I/O outputs.
        void digitalIoInterface_OutputPulsed(object sender, Bytronic.IO.LineLevelChangedEventArgs e)
        {
            this.buttonOutputPulse.SetStateIndex(e.LineLevel ? BTN_OUTPUT_PULSE_STATE_HIGH : BTN_OUTPUT_PULSE_STATE_LOW);
        }
        #endregion


        private void RefreshButtonStates()
        {
            if (this.InvokeRequired)
            {
                RefreshButtonStatesCallback callback = new RefreshButtonStatesCallback(RefreshButtonStates);
                this.Invoke(callback, new object[] { });
            }
            else
            {
                // Digital I/O Class button.
                if (this.digitalIoInterface == null)
                    this.buttonSelectInterfaceClass.SetStateIndex(BTN_IO_CLASS_STATE_NOT_READY);
                else
                    this.buttonSelectInterfaceClass.SetStateIndex(BTN_IO_CLASS_STATE_READY);

                // Start/Stop monitoring.
                if (this.digitalIoInterface == null)
                    this.buttonStartStop.SetStateIndex(BTN_IO_START_STOP_STATE_NOT_READY);
                else if (this.digitalIoInterface.IsMonitoring)
                    this.buttonStartStop.SetStateIndex(BTN_IO_START_STOP_STATE_RUNNING);
                else
                    this.buttonStartStop.SetStateIndex(BTN_IO_START_STOP_STATE_STOPPED);

                // Show/hide monitor.
                if (this.digitalIoInterface == null)
                    this.buttonShowHideMonitor.SetStateIndex(BTN_IO_MONITOR_STATE_NOT_READY);
                else if( this.IsDigitalIoInterfaceWindowVisible)
                    this.buttonShowHideMonitor.SetStateIndex(BTN_IO_MONITOR_STATE_VISIBLE);
                else
                    this.buttonShowHideMonitor.SetStateIndex(BTN_IO_MONITOR_STATE_HIDDEN);
            }
        }


        void digitalIoInterface_ErrorReported(object sender, EventArgs e)
        {
            this.ShowError(this.digitalIoInterface.LastError);
        }


        public bool IsDigitalIoInterfaceWindowVisible
        {
            get { return ((this.digitalIoWindow != null) && !this.digitalIoWindow.IsDisposed); }
        }


        private void DestroyDigitalIoInterfaceWindow()
        {
            if (this.IsDigitalIoInterfaceWindowVisible)
            {
                this.digitalIoWindow.Close();
                this.digitalIoWindow = null;
            }
        }


        private void DestroyDigitalIoInterface()
        {
            if (this.digitalIoInterface != null)
            {
                this.digitalIoInterface.ErrorReported -= digitalIoInterface_ErrorReported;
                this.digitalIoInterface.StopMonitoring();
                this.digitalIoInterface = null;
            }

            this.RefreshButtonStates();
        }


        private void buttonClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        private void DigitalIoTesterWindow_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.DestroyDigitalIoInterfaceWindow();

            this.DestroyDigitalIoInterface();
        }


        private void ShowMessage(string messageText)
        {
            if (this.statusLabel1.InvokeRequired)
            {
                ShowMessageCallback callback = new ShowMessageCallback(ShowMessage);
                this.Invoke(callback, new object[] { messageText });
            }
            else
                this.statusLabel1.Text = messageText;
        }


        private void ShowError(string messageText)
        {
            if (this.statusLabel1.InvokeRequired)
            {
                ShowErrorCallback callback = new ShowErrorCallback(ShowError);
                this.Invoke(callback, new object[] { messageText });
            }
            else
                this.statusLabel1.Text = messageText;
        }


        private void buttonStartStop_Click(object sender, EventArgs e)
        {
            if (this.digitalIoInterface == null)
                this.ShowError("No I/O interface has been established.");
            else if (this.digitalIoInterface.IsMonitoring)
                this.digitalIoInterface.StopMonitoring();
            else
                this.digitalIoInterface.StartMonitoring();

            this.RefreshButtonStates();
        }


        private void buttonShowHideMonitor_Click(object sender, EventArgs e)
        {
            if (this.digitalIoInterface == null)
                this.ShowError("I/O interface not created.");
            else if (this.IsDigitalIoInterfaceWindowVisible)
                this.DestroyDigitalIoInterfaceWindow();
            else
            {
                this.digitalIoWindow = new Bytronic.GUI.DigitalIoWindow();
                this.digitalIoWindow.Icon = this.Icon;
                this.digitalIoWindow.StartPosition = FormStartPosition.Manual;
                this.digitalIoWindow.Location = new Point(this.Location.X + this.Size.Width, this.Location.Y);
                this.digitalIoWindow.DigitalIoInterface = this.digitalIoInterface;
                this.digitalIoWindow.FormClosed += digitalIoWindow_FormClosed;
                this.digitalIoWindow.Show();
            }

            this.RefreshButtonStates();
        }


        void digitalIoWindow_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.digitalIoWindow = null;

            this.RefreshButtonStates();
        }

    }
}
